<?php session_start(); 
require_once 'config/database.php';
include 'site_helper/functions.php';
valida_logado();

$id=(@$_GET['id']);
//echo $id;
$id =  decrypted($id,'123',true) ;

$fetch =get_empresaByid($conn,$id);

 
 ?>
 <!DOCTYPE HTML>
<html>
<head>
<title>Derruba</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Derruba" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/styleForm.css" rel='stylesheet' type='text/css' />
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

 <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 
<!-- Custom Theme files -->
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<!-- start menu -->
<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- the jScrollPane script -->
		<script type="text/javascript" id="sourcecode">
			$(function()
			{
				$('.scroll-pane').jScrollPane();
			});
		</script>
 
</head>
<body>
<div class="header_top">
	<div class="container">
		 
         <div class="cssmenu">
			<ul>
			   <li class="active"><a class="text-white" href="#"> Olá Seja Bem-vindo, <?=$fetch['nome'];?> </a></li> 
			</ul>
		 </div>
	</div>
</div>	
<?php
include ('includes/menu.php');
$empresa_id=$fetch['id'];
$result = get_ganhadores($fetch['id'],$conn);
$rowsConf = get_config($conn);
$tempoExpira = empty($rowsConf[0]['tempo_expiraca_cod']) ? 24 : $rowsConf[0]['tempo_expiraca_cod'];
?>
<!-- https://freefrontend.com/bootstrap-tables/ -->
<div class="container">
 
 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<body>
  
    <div class="container mt-1">
      <!-- Table -->
     
      <!-- Dark table -->
    
        <div class="col-xs-12">
          
            <div class="card-header bg-transparent border-0">
              <h3 class="mb-0">Validar Usuários</h3>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="text-white">
                  <tr>
                    <th scope="col">Imagem</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Ganhador</th>
                    <th scope="col">Código</th>
                    <th scope="col">Data/Hora</th>
                    <th scope="col">Status</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                	<?php 
                  $timenow = date("Y-m-d H:i:s");

                	while( $row =mysqli_fetch_array($result) ) {  

                    $hourdiff = round((strtotime($timenow) - strtotime($row['data']))/3600, 2);
                    //$hourdiff = round(( strtotime($row['data']) - strtotime($timenow)  )/3600, 2); local
                    
                    preg_match('/-(expirado)-/', $row['cod_hash'], $match_e);
                    preg_match('/-(validado)-/', $row['cod_hash'], $match_v);
         
                    $ifexp= (@$match_e[1] =='expirado') ? true : false;
                    $ifval= (@$match_v[1] =='validado') ? true : false;          
 
                    if ($hourdiff > $tempoExpira ) {
                           if ($ifval  == false ) {
                              if ($ifexp == false ) { 
                                 expira_brind( $row['brinde_id'], $row['celular'], $row['cod_hash'],$empresa_id ,$conn);
                              }
                           }
                       }

                	 ?>
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="<?=$url_site_derruba . "/assets/img/brindes/icone/".$row['iconebrinde']  ?>">
                        </a>
                        <div class="media-body">
                          <span class="mb-0 text-sm"> <?=$row['titulo'] ?></span>
                        </div>
                      </div>
                    </th>
                    <td>
                      <?=$row['valor'] ?>
                    </td> 
                    <td>
                      <?= getlast($row['celular'],"#", 4); ?>
                    </td> 
                    <td>
                      <b><?=$row['cod_hash'] ?></b>
                    </td>
                    <td>
                      <?= date('d/m/Y h:m:i', strtotime($row['data'] )); 
                       $hourdiff = round((  strtotime($timenow) - strtotime($row['data'])   )/3600, 2);
                       //$hourdiff = round(( strtotime($row['data']) - strtotime($timenow)  )/3600, 2); local
                       echo "<br> Tempo passado: " . str_replace(".", ",", $hourdiff)  .'h';
                       ?>
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                      	<?php 
                      	if ( strpos($row['cod_hash'], '-validado-')   ) {
                      		$script="javascript:alert('Brinde já validado!')";
                      	 ?>
                      		 <i class="bg-success"></i> Validado!
                      	<?php }elseif( strpos($row['cod_hash'], '-expirado-')  ) { 
                          $script="javascript:alert('Brinde Expirou!')";
                      		?>
                          <i class="bg-info"></i> Expirado.
                      <?php	} else{ 
                        //$script= "javascript:if(confirm('Você deseja validar mesmo esse brinde desse celular, {$row['celular']} ?')) validar({$row['brinde_id']},{$fetch['id']},{$row['celular']},'{$row['cod_hash']}')";
                        $script= "javascript:if(confirm('Atenção: se as compras foi a partir do valor combinado, antes de confirmar a validação confira se o código do usuário presente é esse: {$row['cod_hash']} e clique em ok, ou cancelar.')) validar({$row['brinde_id']},{$fetch['id']},{$row['celular']},'{$row['cod_hash']}')";
                       ?>
                        	<i class="bg-warning"></i> Pendente..
                        <?php } ?> 
                      </span>
                    </td>
                    <td>
                      
                    </td>
                    <td class="text-right">
                      <div class="dropdown">
                        <button class="btn btn-sm">

                         <a class="btn btn-sm" href="<?=$script ?>" role="button"  >Validar</a>
                         </button>
                         
                      </div>
                    </td>
                  </tr>
              <?php } ?>

              
                </tbody>
              </table>
            </div>
          
        </div>
      
    </div>
 </body>

		 
	</div>
 

	</section> 
	  </div>
   </div>
	<!-- start content -->
   <div class="clearfix"></div>
   <!-- end content -->
 </div>
</div>
  <br>
<div class="footer" style="background:#334365;color: white;">
	<div style="width: 100%;margin-right: auto;margin-left: auto;padding-right: 15px;padding-left: 15px;">
	   <div style="margin-bottom: 2em;">
		<div class="col-md-4 box_3">
			<h3>Derruba</h3>
			<address style="margin-bottom: 20px;font-style: normal;line-height: 1.42857143;">
              <p>Ganhe brindes e descontos de maneira simples e divertida usando apenas o número do seu celular, basta aguardar a data e o horário de liberação, após é só clicar no ícone do brinde e digitar o seu celular com o DDD. Aquele que conseguir pegar primeiro ganha, veja se o DERRUBA já está disponível na sua região e boa sorte.</p>
              <dl>
       <!--           <dd>Freephone:<span> +1 800 254 2478</span></dd>
                 <dd>Telephone:<span> +1 800 547 5478</span></dd>
                 <dd>FAX: <span>+1 800 658 5784</span></dd>
                 <dd>E-mail:&nbsp; <a href="mailto@example.com">info(at)buyshop.com</a></dd> -->
              </dl>
           </address>
           <ul class="footer_social">
			  <li><a href=""> <i class="fb"> </i> </a></li>
			  <li><a href=""><i class="tw"> </i> </a></li>
			  <li><a href=""><i class="google"> </i> </a></li>
			  <li><a href=""><i class="instagram"> </i> </a></li>
		   </ul>
		</div>
		<div class="col-md-4 box_3">
			<h3>Contato</h3>
			<h4><a href="#">Entre em contato para avaliarmos sua marca</a></h4>
		 <p class="mb-0"><i class="fab fa-whatsapp mr-2" style="font-size:29px" ></i><a href="https://api.whatsapp.com/send?phone=5575998121860&text=JPUBLICIDADE%20%2F%20DERRUBA" target="_blank"> (75)99812-1860</a> </p>
        <br>  
         <p><i class="fa fa-paper-plane mr-2" style="font-size:20px"></i> aocliente@yahoo.com.br </p>
               <div class="row">
      <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook" style="list-style-type: none;display: inline;padding: 0 5px;" ><a data-toggle="tooltip" href="https://www.facebook.com/vemproderruba" target="_blank" title="" data-original-title="Facebook"  ><i class="fab fa-facebook-f fa-2x"></i></a></li>
            <li class="social-icons-instagran" style="list-style-type: none;display: inline;padding: 0 5px;"><a data-toggle="tooltip" href="https://www.instagram.com/vemproderruba/" target="_blank" title="" data-original-title="Twitter"  ><i class="fab fa-instagram fa-2x"></i></a></li>
      </ul>
    </div>
		</div>
 
		<div class="col-md-4 box_3">
			<h3>Ações</h3>
			<ul class="list_1">
				<li><a href="#">Uma agência disponível com diversos serviços entre outros e focado no crescimento da sua marca.</a></li>
				<li><a href="#">FAQ</a></li>
		 
			</ul>
			<ul class="list_1">
				<li><a href="formulario.php">Formulário de validação</a></li>
				<li><a href="#">About Us</a></li>
			</ul>
			<div class="clearfix"> </div>
		</div>
		<div class="clearfix"> </div>
		</div>
		<div style="border-top: 1px solid rgb(236, 236, 236);padding: 2em 0;">
			<div class="copy">
                <p>© 2019. All Rights Reserved site Derruba.<!-- <a href="http://w3layouts.com/" target="_blank">W3layouts</a> --> </p>
	        </div>
	    </div>
	</div>
</div>
</body>

<script type="text/javascript">
	function validar(brin_id,emp_id,cel,codigo){

      $.ajax({
        type:'POST',
        url: 'site_helper/ajaxresponse.php',
         data: {
         	empresa_id:emp_id,
         	brinde_id:brin_id,
          celular:cel,
          hash:codigo
         } ,
            beforeSend: function(){
                $("#page-overlay").css({'display':'block'});
            },
            success: function(data)
            {  

              var result = JSON.parse(data);
              if (result.status == 'success') {
                    
                    alert(result.message);
                    location.reload();

                    
                }else if(result.status == 'error'){
                      
                    alert(result.message)
                    location.reload();

                }    
             
            },
            error: function(data )
            {   
                 console.debug(data);
            }
        });
        return false;
}

</script>


</html>		