<?php 
session_start();
require_once 'config/database.php';
include 'site_helper/functions.php';
if (empty($_SESSION)) {
	header("location:login.php");
}

 ?>
 <!DOCTYPE HTML>
<html>
<head>
<title>Derrba</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Buy_shop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<script src="js/simpleCart.min.js"> </script>
<!-- Custom Theme files -->
<!--webfont-->

<!--BOTS4-->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<!--BOTS4-->


<link href='http://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>

<!-- start menu -->
<script src="js/jquery.easydropdown.js"></script>
<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<link rel="stylesheet" href="css/etalage.css">
 
<script src="js/jquery.etalage.min.js"></script>
<script>
			jQuery(document).ready(function($){

				$('#etalage').etalage({
					thumb_image_width: 300,
					thumb_image_height: 400,
					source_image_width: 900,
					source_image_height: 1200,
					show_hint: true,
					click_callback: function(image_anchor, instance_id){
						alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
					}
				});

			});
		</script>
<!--initiate accordion-->
<script type="text/javascript">
	$(function() {
	
	    var menu_ul = $('.menu_drop > li > ul'),
	           menu_a  = $('.menu_drop > li > a');
	    
	    menu_ul.hide();
	
	    menu_a.click(function(e) {
	        e.preventDefault();
	        if(!$(this).hasClass('active')) {
	            menu_a.removeClass('active');
	            menu_ul.filter(':visible').slideUp('normal');
	            $(this).addClass('active').next().stop(true,true).slideDown('normal');
	        } else {
	            $(this).removeClass('active');
	            $(this).next().stop(true,true).slideUp('normal');
	        }
	    });
	
	});
</script>
</head>
<body>
<div class="header_top">
	<div class="container">
         <div class="cssmenu">
			<ul>
			   <li class="active"><a href="#"> Olá Seja Bem-vindo, <?=$_SESSION['emp_nome'] ;?> </a></li> 
			</ul>
		 </div>
	</div>
</div>	
 <?php
include ('includes/menu.php');

$prod_det = get_produtos_detalhes($_SESSION['emp_id'] ,$conn,$_GET['id']);
 
?>
<div class="single_top">
	 <div class="container"> 
	      <div class="single_grid">
				<div class="grid images_3_of_2">
						<ul id="etalage">
							<li>
								<a href="optionallink.html">
									<img class="etalage_thumb_image" src="http://derruba.com/assets/img/produtos/<?=$prod_det[0]['imagem'] ?>" class="img-responsive" />
									<img class="etalage_source_image" src="http://derruba.com/assets/img/produtos/<?=$prod_det[0]['imagem'] ?>" class="img-responsive" title="" />
								</a>
							</li>
							<?php if (!empty($prod_det[0]['imagem2'])) { ?>
							<li>
								<img class="etalage_thumb_image" src="http://derruba.com/assets/img/produtos/<?=$prod_det[0]['imagem2']?>" class="img-responsive" />
								<img class="etalage_source_image" src="http://derruba.com/assets/img/produtos/<?=$prod_det[0]['imagem2']?>" class="img-responsive" title="" />
							</li>
						   <?php } ?>
						   <?php if (!empty($prod_det[0]['imagem3'])) { ?>
							<li>
								<img class="etalage_thumb_image" src="http://derruba.com/assets/img/produtos/<?=$prod_det[0]['imagem3'] ?>" class="img-responsive"  />
								<img class="etalage_source_image" src="http://derruba.com/assets/img/produtos/<?=$prod_det[0]['imagem3'] ?>"class="img-responsive"  />
							</li>
						    <?php } ?>
						</ul>
						 <div class="clearfix"></div>		
				  </div> 
				  <div class="desc1 span_3_of_2">
				  	<ul class="back">
                	  <li><i class="back_arrow"> </i>Voltar para <a href="home.php">Home</a></li>
                    </ul>
					<h1><?= ucfirst($prod_det[0]['nome']) ?></h1>
					<p>   <?=$prod_det[0]['descricao'] ?> </p>
				     <div class="single_social_top">
				       <div class="dropdown_left" style="width:70%;" >
					    <p>Nesta compra o Derruba irá receber <b><?=$prod_det[0]['perc_derruba']?></b>  e o usuário receberá um desconto de  <b><?=$prod_det[0]['perc_usuario']?></b></p>
			            </div>

						 <div class="clearfix"></div>
			         </div>
			         <div class="simpleCart_shelfItem">
			         	<div class="price_single">
						  <div class="head"><h2><span class="amount item_price">$<?=$prod_det[0]['preco'] ?> </span></h2></div>
						  <div class="head_desc"><a href="#">12 reviews</a><img src="images/produtos/review.png" alt=""/></li></div>
					       <div class="clearfix"></div>
					     </div>
			               <!--<div class="single_but"><a href="" class="item_add btn_3" value=""></a></div>-->
			              <div class="size_2-right"><a href=""  data-toggle="modal" data-target="#myModalcadastro" class="item_add item_add1 btn_5" value="" />Dar baixa na compra </a></div>
			              <button class="demo btn btn-primary btn-lg" data-toggle="modal" href="#stack1">View Demo</button>
			          


			         </div>
				</div>
          	    <div class="clearfix"></div>
          	   </div>
          	 <div class="single_social_top">   
          	  <ul class="menu_drop">
				<li class="item1"><a href="#"><img src="images/produtos/product_arrow.png">Descrição da Compra</a>
					<ul>
						<li class="subitem1"><a href="#">
						Nesta compra o Derruba irá receber <b><?=$prod_det[0]['perc_derruba']?></b>  e o usuário receberá um desconto de  <b><?=$prod_det[0]['perc_usuario']?></b>
					</a>
				     </li>
						 
					</ul>
				</li>
				<?php if (isset($prod_det[0]['info_add'])) { ?>
				<li class="item2"><a href="#"><img src="images/produtos/product_arrow.png">informações Adicionais </a>
					<ul>
					    <li class="subitem2"><a href="#"> <?=$prod_det[0]['info_add'] ?></a></li>
					</ul>
				</li>
			    <?php } ?>

			 
	 		</ul>
			 </div>

			             <!-- modal -->
	 <div class="modal fade" id="myModalcadastro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
			            <!-- modal -->

   </div>
 <!--  <h3 class="m_2">Related Products</h3>
 	     <div class="container">
          		<div class="box_3">
          			<div class="col-md-3">
          				<div class="content_box"><a href="single.html">
			   	          <img src="images/produtos/pic6.jpg" class="img-responsive" alt="">
				   	   </a>
				   </div>
				    <h4><a href="single.html">Contrary to popular belief</a></h4>
				    <p>$ 199</p>
			        </div>
          			<div class="col-md-3">
          				<div class="content_box"><a href="single.html">
			   	          <img src="images/produtos/pic2.jpg" class="img-responsive" alt="">
				   	   </a>
				   </div>
				    <h4><a href="single.html">Contrary to popular belief</a></h4>
				    <p>$ 199</p>
			        </div>
          			<div class="col-md-3">
          				<div class="content_box"><a href="single.html">
			   	          <img src="images/produtos/pic4.jpg" class="img-responsive" alt="">
				   	   </a>
				   </div>
				    <h4><a href="single.html">Contrary to popular belief</a></h4>
				    <p>$ 199</p>
			        </div>
          			<div class="col-md-3">
          				<div class="content_box"><a href="single.html">
			   	          <img src="images/produtos/pic5.jpg" class="img-responsive" alt="">
				   	   </a>
				   </div>
				    <h4><a href="single.html">Contrary to popular belief</a></h4>
				    <p>$ 199</p>
			        </div>
			        <div class="clearfix"> </div>
          		</div>
          	</div> -->
        </div>
<div class="footer">
	<div class="container">
	   <div class="footer_top">
		<div class="col-md-4 box_3">
			<h3>Our Stores</h3>
			<address class="address">
              <p>9870 St Vincent Place, <br>Glasgow, DC 45 Fr 45.</p>
              <dl>
                 <dt></dt>
                 <dd>Freephone:<span> +1 800 254 2478</span></dd>
                 <dd>Telephone:<span> +1 800 547 5478</span></dd>
                 <dd>FAX: <span>+1 800 658 5784</span></dd>
                 <dd>E-mail:&nbsp; <a href="mailto@example.com">info(at)buyshop.com</a></dd>
              </dl>
           </address>
           <ul class="footer_social">
			  <li><a href=""> <i class="fb"> </i> </a></li>
			  <li><a href=""><i class="tw"> </i> </a></li>
			  <li><a href=""><i class="google"> </i> </a></li>
			  <li><a href=""><i class="instagram"> </i> </a></li>
		   </ul>
		</div>
		<div class="col-md-4 box_3">
			<h3>Blog Posts</h3>
			<h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
			<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
			<h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
			<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
			<h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
			<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
		</div>
		<div class="col-md-4 box_3">
			<h3>Support</h3>
			<ul class="list_1">
				<li><a href="#">Terms & Conditions</a></li>
				<li><a href="#">FAQ</a></li>
				<li><a href="#">Payment</a></li>
				<li><a href="#">Refunds</a></li>
				<li><a href="#">Track Order</a></li>
				<li><a href="#">Services</a></li>
			</ul>
			<ul class="list_1">
				<li><a href="#">Services</a></li>
				<li><a href="#">Press</a></li>
				<li><a href="#">Blog</a></li>
				<li><a href="#">About Us</a></li>
				<li><a href="#">Contact Us</a></li>
			</ul>
			<div class="clearfix"> </div>
		</div>
		<div class="clearfix"> </div>
		</div>
		<div class="footer_bottom">
			<div class="copy">
                <p>Copyright © 2015 Buy_shop. All Rights Reserved.<a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
	        </div>
	    </div>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("#myModalcadastro").modal();
  });
</script>
</body>
</html>		