<?php session_start();

require_once 'config/database.php';
include 'site_helper/functions.php';
 

if (@$_GET['session']) {
	session_destroy();
	$_SESSION = array();
}


// get email
if ($_POST) {
 	
 //$id =$_GET['session'];	
 //echo "id ". $id;
 if (  logar_empresa( $_POST, $conn)  ) {
 	
    $array = get_empresa_hash($_SESSION['emp_email'],$conn);
	 $nome  = urlencode($array['id'] );


	 if($array['count'] > 0){

	    //header("location:home.php");
	    if ($_SERVER['SERVER_NAME'] == 'localhost' ){

	    	header('Location: http://localhost/empresa/validate.php?id='.$nome );
	    	exit();
	      //echo "<script> location.href = 'http://localhost/empresa/validate.php?id={$nome}'; </script>";
	    } 
		 else{
	      echo "<script> location.href = 'http://empresa.derruba.com/validate.php?id={$nome}'; </script>";

		 }
	    
	    exit();
	}else{
		echo "<h3>Dados inválidos, verifique seu link ou email informado!!</h3>";
	}
 
 }else{
echo "<h3>Dados inválidos, verifique seu link ou email informado!!</h3>";

 }

}

?>
<!DOCTYPE HTML>
<html>
<head>
<title>Derruba</title>
 <link rel="shortcut icon" type="images/x-icon" href="images/thumbnailT.jpg" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Ganhe brindes e descontos de maneira simples e divertida usando apenas o número do seu celular, basta aguardar a data e o horário de liberação, após é só clicar no ícone do brinde e digitar o seu celular com o DDD." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<script src="js/simpleCart.min.js"> </script>
<!-- Custom Theme files -->
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<!-- start menu -->
<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
</head>
<body>
<div class="header_top">
	<div class="container">
		 
         <div class="cssmenu">
			<ul>
			   <li class="active"><a href="login.html"></a></li> 
			</ul>
		 </div>
	</div>
</div>	
<?php
include('includes/menu.php');
?>
<div class="single_top">
	 <div class="container"> 
	 	 
	    <div class="register">
	    	
			  <div class="col-md-6 login-right">
			  	<h3>CADASTRO DE EMPRESA</h3>
				<p>Se você tem uma conta conosco, faça o login.</p>
				<form action="login.php" method="POST" name="forma">
				  <div>	
					<span>Endereço de e-mail<label>*</label></span>
					<input type="text" name="email" > 
				  </div>
				  <div>
					<span>Senha<label>*</label></span>
					<input type="text" name="senha"> 
				  </div>
				 <!--  <a class="forgot" href="#">Forgot Your Password?</a> -->
				  <input type="submit" value="Login">
			    </form>
			   </div>
			    <div class="col-md-6 login-left">
			  	 <h3>NOVOS CLIENTES</h3>
				 <p> Ao criar uma conta em nossa loja, você poderá passar pelo processo de finalização de compra mais rapidamente, armazenar vários endereços de remessa, visualizar e rastrear seus pedidos em sua conta e muito mais.</p>
				 <a class="acount-btn" href="register.php">CRIE SUA CONTA</a>
			   </div>	
			   <div class="clearfix"> </div>
		</div>
     </div>
</div>      
<div class="footer">
	<div class="container">
	   <div class="footer_top">
		<div class="col-md-4 box_3">
			<h3>Our Stores</h3>
			<address class="address">
              <p>9870 St Vincent Place, <br>Glasgow, DC 45 Fr 45.</p>
              <dl>
                 <dt></dt>
                 <dd>Freephone:<span> +1 800 254 2478</span></dd>
                 <dd>Telephone:<span> +1 800 547 5478</span></dd>
                 <dd>FAX: <span>+1 800 658 5784</span></dd>
                 <dd>E-mail:&nbsp; <a href="mailto@example.com">info(at)buyshop.com</a></dd>
              </dl>
           </address>
           <ul class="footer_social">
			  <li><a href=""> <i class="fb"> </i> </a></li>
			  <li><a href=""><i class="tw"> </i> </a></li>
			  <li><a href=""><i class="google"> </i> </a></li>
			  <li><a href=""><i class="instagram"> </i> </a></li>
		   </ul>
		</div>
		<div class="col-md-4 box_3">
			<h3>Blog Posts</h3>
			<h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
			<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
			<h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
			<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
			<h4><a href="#">Sed ut perspiciatis unde omnis</a></h4>
			<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</p>
		</div>
		<div class="col-md-4 box_3">
			<h3>Support</h3>
			<ul class="list_1">
				<li><a href="#">Terms & Conditions</a></li>
				<li><a href="#">FAQ</a></li>
				<li><a href="#">Payment</a></li>
				<li><a href="#">Refunds</a></li>
				<li><a href="#">Track Order</a></li>
				<li><a href="#">Services</a></li>
			</ul>
			<ul class="list_1">
				<li><a href="#">Services</a></li>
				<li><a href="#">Press</a></li>
				<li><a href="#">Blog</a></li>
				<li><a href="#">About Us</a></li>
				<li><a href="#">Contact Us</a></li>
			</ul>
			<div class="clearfix"> </div>
		</div>
		<div class="clearfix"> </div>
		</div>
		<div class="footer_bottom">
			<div class="copy">
                <p>Copyright © 2015 Buy_shop. All Rights Reserved.<a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
	        </div>
	    </div>
	</div>
</div>
</body>
</html>		