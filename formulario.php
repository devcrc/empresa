<?php session_start();

if (empty($_SESSION['emp_email']) && empty($_SESSION['emp_id']) ) {
	header("location:login.php?erro=sessionempty");
	die();
}
 if ($_POST) {
  getAll_empresa($_POST);
}
require_once 'config/database.php';
include 'site_helper/functions.php';


 
 ?>
 <!DOCTYPE HTML>
<html>
<head>
<title>Derruba</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Buy_shop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<script src="js/simpleCart.min.js"> </script>
<!-- Custom Theme files -->
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<!-- start menu -->
<script src="js/jquery.easydropdown.js"></script>
<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<link rel="stylesheet" href="css/etalage.css">
<script src="js/jquery.etalage.min.js"></script>
<script>
			jQuery(document).ready(function($){

				$('#etalage').etalage({
					thumb_image_width: 300,
					thumb_image_height: 400,
					source_image_width: 900,
					source_image_height: 1200,
					show_hint: true,
					click_callback: function(image_anchor, instance_id){
						alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
					}
				});

			});
		</script>
<!--initiate accordion-->
<script type="text/javascript">
	$(function() {
	
	    var menu_ul = $('.menu_drop > li > ul'),
	           menu_a  = $('.menu_drop > li > a');
	    
	    menu_ul.hide();
	
	    menu_a.click(function(e) {
	        e.preventDefault();
	        if(!$(this).hasClass('active')) {
	            menu_a.removeClass('active');
	            menu_ul.filter(':visible').slideUp('normal');
	            $(this).addClass('active').next().stop(true,true).slideDown('normal');
	        } else {
	            $(this).removeClass('active');
	            $(this).next().stop(true,true).slideUp('normal');
	        }
	    });
	
	});
</script>
</head>
<body>
<div class="header_top">
	<div class="container">
		 
         <div class="cssmenu">
			<ul>
			   <li class="active"><a href="#"> Olá Seja Bem-vindo, <?=$_SESSION['emp_nome'] ;?> </a></li> 
			</ul>
		 </div>
	</div>
</div>	
<?php
include ('includes/menu.php');
?>
<div class="container">
<div class="women_main">
	<div class="col-md-9 w_content">
	    <div class="women">
			<a href="#"><h4>Total - <span><?= get_produtos($_SESSION['emp_id'] ,$conn, true); ?> Ganhadores</span> </h4></a>
			 
		     <div class="clearfix"></div>	
		</div>
		<!-- grids_of_4 -->
		 
		 
			<div class="clearfix"></div>
		</div>
 
		<!-- end grids_of_4 -->
	</div>
	<!-- start sidebar -->

	<!-- start content -->
   <div class="clearfix"></div>
   <div class="col-md-9 contact_left">
		 	  <h1>Formulário de consulta</h1>
	  			  <p>Aqui você pode consultar quais foram os ganhadores pelo número e o código cedido pelo cliente.</p>
	  			 <form action="" method="" >
					 
			     <div class="column_2">
				       <div class="dropdown_left">
					     <select class="dropdown" tabindex="10" data-settings='{"wrapperClass":"metro1"}'>
	            			<option value="0">Celular</option>	
							<option value="1">9999878</option>
							<option value="2">11545455</option>
							<option value="3">88787888</option>
			             </select>
			            </div>
			       </div>
   					<div class="column_2">
					 	<input type="text" class="text" placeholder="" value="Código verificador" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Subject';}" style="margin-left:2.7%">
	                </div>
					
	                <div class="form-submit1">
			          <input type="submit" value="Consultar">
					</div>
					<div class="clearfix"> </div>
				  </form>
		 </div>
   <!-- end content -->
 </div>
</div>
  <br>
<div class="footer">
	<div class="container">
	   <div class="footer_top">
		<div class="col-md-4 box_3">
			<h3>Derruba</h3>
			<address class="address">
              <p>Ganhe brindes e descontos de maneira simples e divertida usando apenas o número do seu celular, basta aguardar a data e o horário de liberação, após é só clicar no ícone do brinde e digitar o seu celular com o DDD. Aquele que conseguir pegar primeiro ganha, veja se o DERRUBA já está disponível na sua região e boa sorte.</p>
              <dl>
       <!--           <dd>Freephone:<span> +1 800 254 2478</span></dd>
                 <dd>Telephone:<span> +1 800 547 5478</span></dd>
                 <dd>FAX: <span>+1 800 658 5784</span></dd>
                 <dd>E-mail:&nbsp; <a href="mailto@example.com">info(at)buyshop.com</a></dd> -->
              </dl>
           </address>
           <ul class="footer_social">
			  <li><a href=""> <i class="fb"> </i> </a></li>
			  <li><a href=""><i class="tw"> </i> </a></li>
			  <li><a href=""><i class="google"> </i> </a></li>
			  <li><a href=""><i class="instagram"> </i> </a></li>
		   </ul>
		</div>
		<div class="col-md-4 box_3">
			<h3>Contato</h3>
			<h4><a href="#">Entre em contato para avaliarmos sua marca</a></h4>
		 <p class="mb-0"><i class="fab fa-whatsapp mr-2" style="font-size:29px" ></i><a href="https://api.whatsapp.com/send?phone=5575998121860&text=JPUBLICIDADE%20%2F%20DERRUBA" target="_blank"> (75)99812-1860</a> </p>
        <br>  
         <p><i class="fa fa-paper-plane mr-2" style="font-size:20px"></i> aocliente@yahoo.com.br </p>
               <div class="row">
      <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook" style="list-style-type: none;display: inline;padding: 0 5px;" ><a data-toggle="tooltip" href="https://www.facebook.com/vemproderruba" target="_blank" title="" data-original-title="Facebook"  ><i class="fab fa-facebook-f fa-2x"></i></a></li>
            <li class="social-icons-instagran" style="list-style-type: none;display: inline;padding: 0 5px;"><a data-toggle="tooltip" href="https://www.instagram.com/vemproderruba/" target="_blank" title="" data-original-title="Twitter"  ><i class="fab fa-instagram fa-2x"></i></a></li>
      </ul>
    </div>
		</div>
 
		<div class="col-md-4 box_3">
			<h3>Ações</h3>
			<ul class="list_1">
				<li><a href="#">Uma agência disponível com diversos serviços entre outros e focado no crescimento da sua marca.</a></li>
				<li><a href="#">FAQ</a></li>
		 
			</ul>
			<ul class="list_1">
				<li><a href="formulario.php">Formulário de validação</a></li>
				<li><a href="#">About Us</a></li>
			</ul>
			<div class="clearfix"> </div>
		</div>
		<div class="clearfix"> </div>
		</div>
		<div class="footer_bottom">
			<div class="copy">
                <p>© 2019. All Rights Reserved site Derruba.<!-- <a href="http://w3layouts.com/" target="_blank">W3layouts</a> --> </p>
	        </div>
	    </div>
	</div>
</div>
</body>
</html>		