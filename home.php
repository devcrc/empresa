<?php session_start();

if (empty($_SESSION['emp_email']) && empty($_SESSION['emp_id']) ) {
	header("location:login.php?erro=sessionempty");
	die();
}
 if ($_POST) {
  getAll_empresa($_POST);
}
require_once 'config/database.php';
include 'site_helper/functions.php';


 
 ?>
 <!DOCTYPE HTML>
<html>
<head>
<title>Derruba</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Buy_shop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<script src="js/simpleCart.min.js"> </script>
<!-- Custom Theme files -->
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<!-- start menu -->
<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<!-- the jScrollPane script -->
<script type="text/javascript" src="js/jquery.jscrollpane.min.js"></script>
		<script type="text/javascript" id="sourcecode">
			$(function()
			{
				$('.scroll-pane').jScrollPane();
			});
		</script>
</head>
<body>
<div class="header_top">
	<div class="container">
		 
         <div class="cssmenu">
			<ul>
			   <li class="active"><a href="#"> Olá Seja Bem-vindo, <?=$_SESSION['emp_nome'] ;?> </a></li> 
			</ul>
		 </div>
	</div>
</div>	
<?php
include ('includes/menu.php');
?>
<div class="container">
<div class="women_main">
	<div class="col-md-9 w_content">
	    <div class="women">
			<a href="#"><h4>Total - <span><?= get_produtos($_SESSION['emp_id'] ,$conn, true); ?> itens</span> </h4></a>
			 
		     <div class="clearfix"></div>	
		</div>
		<!-- grids_of_4 -->
		 
		 <?php 
		  $i=1;
		   foreach (get_produtos($_SESSION['emp_id'] ,$conn) as $prod) {
      	 ?>
		<?php  if ( $i% 4== 0) { ?>
		   <div class="grids_of_4">
		<?php } ?>
		  <div class="grid1_of_4 simpleCart_shelfItem">
				<div class="content_box"><a href="produtos-detalhes.php?id=<?=$prod['id_prod'] ?>" >
			   	  <div class="view view-fifth">
			   	   	 <img src="http://derruba.com/assets/img/produtos/<?=$prod['imagem']?>" class="img-responsive" alt=""/>
				   	   	<div class="mask1">
	                        <div class="info">   </div>
			            </div>
				   	  </a> 
				   </div>
				    <h5><a href="produtos-detalhes.php?id=<?=$prod['id_prod']?>"> <?=$prod['nome'] ?></a></h5>
				    <h6> <?=truncate($prod['descricao'],110)  ?></h6>
				     <div class="size_1">
				     	<span class="item_price">$<?=$prod['preco']?></span>
				       <select class="item_Size">
						<option value="Small">L</option>
						<option value="Medium">S</option>
						<option value="Large">M</option>	
						<option value="Large">XL</option>	
		      		    </select>
		      		    <div class="clearfix"></div>
		      		  </div>
		      		  <div class="size_2">
		      		    <div class="size_2-left"> 
					       <input type="text" class="item_quantity quantity_1" value="<?=$prod['quantidade'] ?>" />
					    </div>
			    	    <div class="size_2-right"><input type="button" class="item_add add3" value="" /></div>
			    	    <div class="clearfix"> </div>
			    	 </div>
			  </div>
		</div>
	<?php  if ( $i% 4== 0) { ?>
		   </div>
		<?php } ?>
	<?php 
	$i++;
     } 
     ?> 
			<div class="clearfix"></div>
		</div>
 
		<!-- end grids_of_4 -->
	</div>
	<!-- start sidebar -->
	<div class="col-md-3">
	  <div class="w_sidebar">
	  		<section  class="sky-form">
					<h4>catogories</h4>
		<div class="w_nav1">
			 
			<ul>
				<li><a href="women.html">women</a></li>
				<li><a href="#">new arrivals</a></li>
				<li><a href="#">trends</a></li>
				<li><a href="#">boys</a></li>
				<li><a href="#">girls</a></li>
				<li><a href="#">sale</a></li>
			</ul>	
		</div>

	</section>
	 	<!--
		<section  class="sky-form">
					<h4>Marcas</h4>
						<div class="row1 scroll-pane">
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>kurtas</label>
							</div>
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>kutis</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>churidar kurta</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>salwar</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>printed sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>fashion sari</label>	
							</div>
						</div>
		</section>
	
		<section  class="sky-form">
					<h4>brand</h4>
						<div class="row1 scroll-pane">
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
							</div>
							<div class="col col-4">
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>vishud</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>amari</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
								<label class="checkbox"><input type="checkbox" name="checkbox" ><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>shree</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Anouk</label>
								<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>biba</label>																								
							</div>
						</div>
		</section>
	 	<section class="sky-form">
			<h4>colour</h4>
			<ul class="w_nav2">
				<li><a class="color1" href="#"></a></li>
				<li><a class="color2" href="#"></a></li>
				<li><a class="color3" href="#"></a></li>
				<li><a class="color4" href="#"></a></li>
				<li><a class="color5" href="#"></a></li>
				<li><a class="color6" href="#"></a></li>
				<li><a class="color7" href="#"></a></li>
				<li><a class="color8" href="#"></a></li>
				<li><a class="color9" href="#"></a></li>
				<li><a class="color10" href="#"></a></li>
				<li><a class="color12" href="#"></a></li>
				<li><a class="color13" href="#"></a></li>
				<li><a class="color14" href="#"></a></li>
				<li><a class="color15" href="#"></a></li>
				<li><a class="color5" href="#"></a></li>
				<li><a class="color6" href="#"></a></li>
				<li><a class="color7" href="#"></a></li>
				<li><a class="color8" href="#"></a></li>
				<li><a class="color9" href="#"></a></li>
				<li><a class="color10" href="#"></a></li>
			</ul>
		</section> -->

	<!-- 	<section class="sky-form">
						<h4>discount</h4>
						<div class="row1 scroll-pane">
							<div class="col col-4">
								<label class="radio"><input type="radio" name="radio" checked=""><i></i>60 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>50 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>40 % and above</label>
							</div>
							<div class="col col-4">
								<label class="radio"><input type="radio" name="radio"><i></i>30 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>20 % and above</label>
								<label class="radio"><input type="radio" name="radio"><i></i>10 % and above</label>
							</div>
						</div>						
		</section> -->
	  </div>
   </div>
	<!-- start content -->
   <div class="clearfix"></div>
   <!-- end content -->
 </div>
</div>
  <br>
<div class="footer">
	<div class="container">
	   <div class="footer_top">
		<div class="col-md-4 box_3">
			<h3>Derruba</h3>
			<address class="address">
              <p>Ganhe brindes e descontos de maneira simples e divertida usando apenas o número do seu celular, basta aguardar a data e o horário de liberação, após é só clicar no ícone do brinde e digitar o seu celular com o DDD. Aquele que conseguir pegar primeiro ganha, veja se o DERRUBA já está disponível na sua região e boa sorte.</p>
              <dl>
       <!--           <dd>Freephone:<span> +1 800 254 2478</span></dd>
                 <dd>Telephone:<span> +1 800 547 5478</span></dd>
                 <dd>FAX: <span>+1 800 658 5784</span></dd>
                 <dd>E-mail:&nbsp; <a href="mailto@example.com">info(at)buyshop.com</a></dd> -->
              </dl>
           </address>
           <ul class="footer_social">
			  <li><a href=""> <i class="fb"> </i> </a></li>
			  <li><a href=""><i class="tw"> </i> </a></li>
			  <li><a href=""><i class="google"> </i> </a></li>
			  <li><a href=""><i class="instagram"> </i> </a></li>
		   </ul>
		</div>
		<div class="col-md-4 box_3">
			<h3>Contato</h3>
			<h4><a href="#">Entre em contato para avaliarmos sua marca</a></h4>
		 <p class="mb-0"><i class="fab fa-whatsapp mr-2" style="font-size:29px" ></i><a href="https://api.whatsapp.com/send?phone=5575998121860&text=JPUBLICIDADE%20%2F%20DERRUBA" target="_blank"> (75)99812-1860</a> </p>
        <br>  
         <p><i class="fa fa-paper-plane mr-2" style="font-size:20px"></i> aocliente@yahoo.com.br </p>
               <div class="row">
      <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook" style="list-style-type: none;display: inline;padding: 0 5px;" ><a data-toggle="tooltip" href="https://www.facebook.com/vemproderruba" target="_blank" title="" data-original-title="Facebook"  ><i class="fab fa-facebook-f fa-2x"></i></a></li>
            <li class="social-icons-instagran" style="list-style-type: none;display: inline;padding: 0 5px;"><a data-toggle="tooltip" href="https://www.instagram.com/vemproderruba/" target="_blank" title="" data-original-title="Twitter"  ><i class="fab fa-instagram fa-2x"></i></a></li>
      </ul>
    </div>
		</div>
 
		<div class="col-md-4 box_3">
			<h3>Ações</h3>
			<ul class="list_1">
				<li><a href="#">Uma agência disponível com diversos serviços entre outros e focado no crescimento da sua marca.</a></li>
				<li><a href="#">FAQ</a></li>
		 
			</ul>
			<ul class="list_1">
				<li><a href="formulario.php">Formulário de validação</a></li>
				<li><a href="#">About Us</a></li>
			</ul>
			<div class="clearfix"> </div>
		</div>
		<div class="clearfix"> </div>
		</div>
		<div class="footer_bottom">
			<div class="copy">
                <p>© 2019. All Rights Reserved site Derruba.<!-- <a href="http://w3layouts.com/" target="_blank">W3layouts</a> --> </p>
	        </div>
	    </div>
	</div>
</div>
</body>
</html>		