
<style type="text/css">
	.sair {
    background: #f54d56;
    color: #FFF;
    font-size: 0.8em;
    padding: 0.7em 1.2em;
    transition: 0.5s all;
    -webkit-transition: 0.5s all;
    -moz-transition: 0.5s all;
    -o-transition: 0.5s all;
    display: inline-block;
    text-transform: uppercase;
    border: none;
    outline: none;
}
</style>
 <div class="header_bottom men_border">
	    <div class="container">
			<div class="col-xs-12 header-bottom-left">
				<div class="col-xs-2 logo">
					<a href="validate.php"><img src="images/logo.png" alt="Logo Derruba" title="Logo Derruba" style="width:80px;"></a>
				</div>
				<?php 
	          	if (!empty($_SESSION)) {
				 ?>
					<div class="header-bottom-right" style="margin-top:-25px;">
						<a href="login.php?session=sair" style class="sair">Sair</a>
					</div>
			   <?php } ?>
				<div class="col-xs-6 menu  ">  
		          <ul class="megamenu skyblue" style="display: none;">
	          	<?php 
	          	if (!empty($_SESSION)) {

				foreach (get_category($_SESSION['emp_id'] ,$conn) as $categ) {
	          	 ?>
			      <li class=" grid"><a class="color1" href="#"><?=utf8_encode($categ['nome']) ?></a><div class="megapanel">
					 <div class="row">
						    <div class="col1">
						    	<div class="h_nav">
								<ul>
								<?php 
								foreach (get_subcategory($_SESSION['emp_id'],$categ['id'], $conn) as $subcateg) {
								?>
									 <li><a href="produto?id=<?=$subcateg['id'] ?>"> <?=utf8_encode($subcateg['nome'])?></a></li>			 
							    <?php  } ?>
							  
								</ul>	
							</div>							
						</div>

					  </div>
					</div>
				  </li>
				<?php } ?>
				<?php } ?>
		<!-- 	 	<li class="active grid"><a class="color2" href="#">Moda Mulher- se sql</a>
					  <div class="megapanel">
						<div class="row">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="men.html">Accessories</a></li>
										<li><a href="men.html">Bags</a></li>
										<li><a href="men.html">Caps & Hats</a></li>
										<li><a href="men.html">Hoodies & Sweatshirts</a></li>
										<li><a href="men.html">Jackets & Coats</a></li>
										<li><a href="men.html">Jeans</a></li>
										<li><a href="men.html">Jewellery</a></li>
										<li><a href="men.html">Jumpers & Cardigans</a></li>
										<li><a href="men.html">Leather Jackets</a></li>
										<li><a href="men.html">Long Sleeve T-Shirts</a></li>
										<li><a href="men.html">Loungewear</a></li>
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="men.html">Shirts</a></li>
										<li><a href="men.html">Shoes, Boots & Trainers</a></li>
										<li><a href="men.html">Shorts</a></li>
										<li><a href="men.html">Suits & Blazers</a></li>
										<li><a href="men.html">Sunglasses</a></li>
										<li><a href="men.html">Sweatpants</a></li>
										<li><a href="men.html">Swimwear</a></li>
										<li><a href="men.html">Trousers & Chinos</a></li>
										<li><a href="men.html">T-Shirts</a></li>
										<li><a href="men.html">Underwear & Socks</a></li>
										<li><a href="men.html">Vests</a></li>
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<h4>Popular Brands</h4>
									<ul>
										<li><a href="men.html">Levis</a></li>
										<li><a href="men.html">Persol</a></li>
										<li><a href="men.html">Nike</a></li>
										<li><a href="men.html">Edwin</a></li>
										<li><a href="men.html">New Balance</a></li>
										<li><a href="men.html">Jack & Jones</a></li>
										<li><a href="men.html">Paul Smith</a></li>
										<li><a href="men.html">Ray-Ban</a></li>
										<li><a href="men.html">Wood Wood</a></li>
									</ul>	
								</div>												
							</div>
						  </div>
						</div>
			    </li>   -->
				 
			  </ul> 
			</div>
		</div>
	    <div class="col-xs-4 header-bottom-right">
	    
	        
	         
       </div>
        <div class="clearfix"></div>
	 </div>
</div>